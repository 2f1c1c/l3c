//	Pokemons await/sync

//'use strict'
import express from 'express';
import fetch from 'isomorphic-fetch';
const _ = require('lodash');
//import Promise from 'bluebird';

//By default, a list 'page' will contain up to 20 resources. If you would like to change this just 
//add a 'limit' query param, e.g. limit=60.
//https://pokeapi.co/api/v2/pokemon/?limit=850

const limit = "5";
//const mainUrl = "https://pokeapi.co/api/v2/pokemon/?limit=" + limit;
// Pokeapi local mirror
const mainUrl = "http://192.168.0.103:3003/pokemons";

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



// Get count
async function getCount() {
	const response = await fetch(`${mainUrl}` + "/?offset=" + '0' + "&limit=" + '1');
	const page = await response.json();
	return page.count;
}

// Get all pokemons ?offset=0&limit=1
async function getAllPokemons(offset, count) {
	//const response = await fetch(`${mainUrl}` + "/?offset=" + offset + "&limit=" + count);
	const response = await fetch(`${mainUrl}`);
	const page = await response.json();
	return page.results;
}

// Get one pokemon
async function getOnePokemon(url) {
	const response = await fetch(url);
	const page = await response.json();
	return page;
}


app.get('/*', async (req,res) => {
try {	
	
	let count = req.query['limit'];
	let offset = req.query['offset'];
	if (!count) count = 20;
	if (!offset) offset = 0;

	const page = await getAllPokemons(offset, count);
	const idsPromises = page.map(ids => {
	  let pokeId = ids.url.slice(0,ids.url.length-1);
	  pokeId = pokeId.slice(pokeId.lastIndexOf('/')+1, pokeId.length)
	  return getOnePokemon(`${mainUrl}` + '/' + pokeId);
	});
	
	const allPokemonsInfo = await Promise.all(idsPromises);
	const somePokemonsInfo = allPokemonsInfo.map(pokemon => {
	return {name:pokemon.name, weight: pokemon.weight, height: pokemon.height};
	});
	
  if (req.params[0] === 'heavy') {
	  const sortPokemonsInfo = somePokemonsInfo.sort((a,b) => {
	  return (+a.weight < +b.weight)-(+b.weight < +a.weight)||(b.name<a.name)-(a.name<b.name);
	  });
	} else if (req.params[0] === 'light') {
	  const sortPokemonsInfo = somePokemonsInfo.sort(function(a,b) {
	  return (+b.weight < +a.weight) - (+a.weight< +b.weight) || (b.name<a.name) - (a.name<b.name) ;
	  });
	
	} else if (req.params[0] === 'micro') {
    const sortPokemonsInfo = somePokemonsInfo.sort(function(a,b) {
	  return (+b.height < +a.height) - (+a.height< +b.height) || (b.name<a.name) - (a.name<b.name) ;
	  });
	} else if (req.params[0] === 'huge') {
    const sortPokemonsInfo = somePokemonsInfo.sort(function(a,b) {
	  return  (+a.height< +b.height) - (+b.height < +a.height) || (b.name<a.name) - (a.name<b.name) ;
	  });
	} else if (req.params[0] === 'fat') {
	  const sortPokemonsInfo = somePokemonsInfo.sort(function(a,b) {
	  return  (+a.weight/a.height< +b.weight/b.height) - (+b.weight/b.height < +a.weight/a.height) || (b.name<a.name) - (a.name<b.name) ;
	  });
	} else if (req.params[0] === 'angular') {
	  const sortPokemonsInfo = somePokemonsInfo.sort(function(a,b) {
	  return  (+b.weight/b.height < +a.weight/a.height) - (+a.weight/a.height< +b.weight/b.height)|| (b.name<a.name) - (a.name<b.name) ;
	  });
	} else {
	  const sortPokemonsInfo = somePokemonsInfo.sort(function(a,b) {
	  return  (b.name<a.name) - (a.name<b.name) });
	} 
	
	const anotherPokemonsInfo = somePokemonsInfo.map(pokemon => {
	return {name:pokemon.name};
	});
	
	let i = 0;
	let answer = [];
	for (let key in anotherPokemonsInfo) {
	  answer[i] = anotherPokemonsInfo[key].name;
	  i++;
	}

	let anotherAnswer = []
	let j = 0;
	for (i=offset; i < (+count + +offset); i++) {
	  anotherAnswer[j] = answer[i];
	  j++;

	}

	res.send(anotherAnswer);
    
	} catch(err) {
		console.log(err);
		res.json({err});}
	});


app.listen(3000, function() {
});



